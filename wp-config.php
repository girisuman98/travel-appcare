<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'travel' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']MJ`wRhp5Ce4s~qe{N]1)vT5Tss;]f:HG!j?yAsFg>n)O~PBSV6pq32GJ.8nqi.W' );
define( 'SECURE_AUTH_KEY',  '18mK{Q p@n/v}Uro+WSOC&vtTl(#co(d*<Njr~stoG*eeq`j&ODsPA6Ri7GJg$Io' );
define( 'LOGGED_IN_KEY',    '0l_Ln]{Y~6hj)r~TJTIG,|*MJFE*>o$8eO =LOD4hgP-<F4JfOm,ix>KQYA87>:Q' );
define( 'NONCE_KEY',        'd]Fc%32(ZIQ[A)DoX2{ZvJa w?|w?B+dcp$y+.$b] FU97X03o(F0>p:ws4F6K t' );
define( 'AUTH_SALT',        'R|||w{6CXRc-h+~y1z4<Oe]@wS6Ny,VSu% ^;PT09zjy4[I4INbu| YI8T.*kd{j' );
define( 'SECURE_AUTH_SALT', 'cSl_Hd&NQ9ycOTTDR*gphVW=0gZI%LPT-`,VMa`V86r2D^5<){,%u>QW|AuoH4:q' );
define( 'LOGGED_IN_SALT',   '^~Mn=e-UHl-n-tMr1QWakw3;1 2nQM$uW~~`>%r2uUw$b4S1y?.2vS@V.Oh+zapP' );
define( 'NONCE_SALT',       'K|5sSAh}RlRqoGx5=!-,l[62C5nP@H^+6Kz?MYgJqQ=z8[ot.kQ^MCAxw=,mzba3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
